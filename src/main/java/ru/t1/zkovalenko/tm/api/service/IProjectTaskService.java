package ru.t1.zkovalenko.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    void removeProjectByIndex(String userId, Integer index);

    void unbindTaskFromProject(String userId, String projectId, String taskId);
}
