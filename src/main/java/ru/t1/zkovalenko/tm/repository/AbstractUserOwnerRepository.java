package ru.t1.zkovalenko.tm.repository;

import ru.t1.zkovalenko.tm.api.repository.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.model.AbstractUserOwnerModel;

import java.util.*;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Override
    public void clear(final String userId) {
        List<M> filteredModels = new ArrayList<>();
        for (final M m : models) {
            if (userId.equals(m.getUserId())) filteredModels.add(m);
        }
        for (final M m : filteredModels) {
            removeById(m.getId());
        }
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M m : models) {
            if (userId.equals(m.getUserId())) result.add(m);
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public boolean existById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        for (final M model : models) {
            if (!id.equals(model.getId())) continue;
            if (!userId.equals(model.getUserId())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return models.size() > index ? findAll(userId).get(index) : null;
    }

    @Override
    public int getSize(final String userId) {
        int count = 0;
        for (final M m : models) {
            if (userId.equals(m.getUserId())) count++;
        }
        return count;
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M add(final String userId, M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

}
