package ru.t1.zkovalenko.tm.command.user;

import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    private final String NAME = "user-view-profile";

    private final String DESCRIPTION = "User view profile";

    @Override
    public void execute() {
        System.out.println("[USER VIEW PROFILE]");
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
