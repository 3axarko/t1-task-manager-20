package ru.t1.zkovalenko.tm.command.project;

import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-start-by-id";

    public static final String DESCRIPTION = "Project start by id";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
