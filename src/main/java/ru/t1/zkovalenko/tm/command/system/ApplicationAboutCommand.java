package ru.t1.zkovalenko.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Who did it? And why?";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Who is the best: Read below");
        System.out.println("Developer: 3axarko");
        System.out.println("And he's really good");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
