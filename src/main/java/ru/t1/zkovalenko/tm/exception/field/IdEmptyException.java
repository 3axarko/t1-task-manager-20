package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class IdEmptyException extends AbstractException {

    public IdEmptyException() {
        super("Id is empty");
    }

}
