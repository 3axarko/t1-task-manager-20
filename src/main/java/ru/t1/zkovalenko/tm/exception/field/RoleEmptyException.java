package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class RoleEmptyException extends AbstractException {

    public RoleEmptyException() {
        super("Role is empty");
    }

}
